<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'inicioController@getInicio');

// Route::get('login', function () {
//     return "Login usuario";
// });

// Route::get('logout', function () {
// 	return "Logout usuario";
// });

Route::get('fiestas', 'FiestasController@getTodas');

Route::get('fiestas/ver/{id}', 'FiestasController@getVer');
