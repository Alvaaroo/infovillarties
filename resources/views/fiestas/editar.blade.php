@extends('layouts.master')
@section('titulo')
Editar Veterinario
@endsection
@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Editar mascota
			</div>
			<div class="card-body" style="padding:30px">
				{{-- TODO: Abrir el formulario e indicar el método POST --}}
				<form action="{{url('mascotas/editar')}}/{{$Mascota->id}}" method="POST" enctype="multipart/form-data">
					{{-- TODO: Protección contra CSRF --}}
					{{ csrf_field() }}
					{{-- @for ($i = 0; $i < count($arrayMascotas); $i++)	
						@if($i == $id ?? '') --}}
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" id="nombre" class="form-control" value="{{$Mascota->nombre}}">
						</div>
						<div class="form-group">
							{{-- TODO: Completa el input para la especie --}}
							<label for="nombre">Especie</label>
							<input type="text" name="especie" id="especie" class="form-control" value="{{$Mascota->especie}}">
						</div>
						<div class="form-group">
							{{-- TODO: Completa el input para la raza --}}
							<label for="nombre">Raza</label>
							<input type="text" name="raza" id="raza" class="form-control" value="{{$Mascota->raza}}">
						</div>
						<div class="form-group">
							{{-- TODO: Completa el input para la fecha de nacimiento --}}
							<label for="nombre">Fecha de nacimiento</label>
							<input type="date" name="fechaNacimiento" id="fechaNacimiento" class="form-control" value="{{$Mascota->fechaNacimiento}}">
						</div>
						<div class="form-group">
							{{-- TODO: Completa el input para el cliente --}}
							<label for="nombre">Cliente</label>
							<input type="text" name="cliente" id="cliente" class="form-control" value="{{$Mascota->cliente}}">
						</div>
						<div class="form-group">
							<label for="historial">Historial</label>
							<textarea name="historial" id="historial" class="form-control" rows="3">{{$Mascota->historial}}</textarea>
						</div>
						<div class="form-group">
							{{-- TODO: Completa el input para la imagen --}}
							<label for="nombre">Imagen</label>
							<input type="file" class="form-control-file" id="imagen" name="imagen">
						</div>
						<div class="form-group text-center">
							<a class="btn btn-light" style="padding:8px 100px;margin-top:25px;" href="{{url('/mascotas/ver/' . $Mascota->id ) }}"><img src="http://localhost/DWES/laravel_veterinario/public/assets/imagenes/volver.png" height="20" width="20">Volver</a>

							<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
								Editar mascota
							</button>
						</div>
					{{-- @endif
						@endfor --}}
						{{-- TODO: Cerrar formulario --}}
					</form>
				</div>
			</div>
		</div>
	</div>
	@endsection