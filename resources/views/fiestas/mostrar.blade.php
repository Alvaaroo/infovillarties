@extends('layouts.master')
@section('titulo')
mostrar Fiesta
@endsection
@section('contenido')
<div class="row">	
	<div class="col-sm-3">
		{{-- TODO: Imagen de la mascota --}}
		<img src="http://localhost/DWES/InfoVillarties/public/assets/imagenes/{{$fiestas['imagen']}}" class="fluid" height="637" width="400" />	
	</div>
	<div class="col-sm-9">
		{{-- TODO: Datos de la mascota --}}
		<h2 style="min-height:45px;margin:5px 0 10px 0">{{$fiestas['nombre']}}</h2>		
		<h5>Localización:</h5>
		<p>{{$fiestas['localizacion']}}</p>
		<h5>Fecha/s:</h5>
		<p>{{$fiestas['fechaInicio']}} - {{$fiestas['fechaFin']}}</p>
		<h5>Mapa:</h5>
		<iframe src="{{$fiestas['mapa']}}" width="400" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
		{{-- <h5>Historial</h5> --}}
		{{-- <a class="btn btn-warning" href="{{ url('/mascotas/editar/' . $clave ) }}"><img src="http://localhost/DWES/laravel_veterinario/public/assets/imagenes/editar.png" height="20" width="20">Editar</a> --}}
	</div>
</div>
<a class="btn btn-light" href="{{ action('inicioController@getInicio')}}"><img src="http://localhost/DWES/InfoVillarties/public/assets/imagenes/volver.png" height="20" width="20">Volver</a>
{{-- @endif --}}
{{-- @endforeach --}}
@endsection