@extends('layouts.master')
@section('titulo')
Infovillarties (index)
@endsection
@section('contenido')
{{-- <div class="container"> --}}
	<div class="row">
		@foreach($fiestas as $clave => $fiesta)
		<div style="border-left:1px solid black;" class="col-xs-12 {{-- col-sm-1 --}} col-md-6">
			<img src="assets/imagenes/{{$fiesta['imagen']}}" class='fluid' class="card-img-bottom" height="300" width="400" margin="10" />
			<a href="{{ url('/fiestas/ver/' . $clave ) }}">
				<h4 style="min-height:45px;margin:5px 0 10px 0">
					{{$fiesta['nombre']}}
				</h4>
			</a>
			<p>Localizacion: {{$fiesta['localizacion']}}</p>
			<p>Fecha de {{$fiesta['fechaInicio']}} hasta {{$fiesta['fechaFin']}}</p>
		</div>
		@endforeach
	</div>
{{-- </div> --}}
@endsection