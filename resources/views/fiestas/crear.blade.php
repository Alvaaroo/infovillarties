@extends('layouts.master')
@section('titulo')
Veterinario (crear)
@endsection
@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Crear mascota
			</div>
			<div class="card-body" style="padding:30px">
				{{-- TODO: Abrir el formulario e indicar el método POST --}}
				<form action="{{action('MascotasController@postCrear')}}" method="POST" enctype="multipart/form-data">
					{{-- TODO: Protección contra CSRF --}}
					{{ csrf_field() }}
					<div class="form-group">
						<label for="nombre">Nombre</label>
						<input type="text" name="nombre" id="nombre" class="form-control" value="{{old('nombre')}}">
					</div>
					<div class="form-group">
						{{-- TODO: Completa el input para la especie --}}
						<label for="nombre">Especie</label>
						<input type="text" name="especie" id="especie" class="form-control" value="{{old('especie')}}">
					</div>
					<div class="form-group">
						{{-- TODO: Completa el input para la raza --}}
						<label for="nombre">Raza</label>
						<input type="text" name="raza" id="raza" class="form-control" value="{{old('raza')}}">
					</div>
					<div class="form-group">
						{{-- TODO: Completa el input para la fecha de nacimiento --}}
						<label for="nombre">Fecha de nacimiento</label>
						<input type="date" name="fechaNacimiento" id="fechaNacimiento" class="form-control" value="{{old('fechaNacimiento')}}">
					</div>
					<div class="form-group">
						{{-- TODO: Completa el input para el cliente --}}
						<label for="nombre">Cliente</label>
						<input type="text" name="cliente" id="cliente" class="form-control" value="{{old('cliente')}}">
					</div>
					<div class="form-group">
						<label for="historial">Historial</label>
						<textarea name="historial" id="historial" class="form-control" rows="3">{{old('historial')}}</textarea>
					</div>
					<div class="form-group">
						{{-- TODO: Completa el input para la imagen --}}
						<label for="nombre">Imagen</label>
						<input type="file" class="form-control-file" id="imagen" name="imagen" value="{{old('imagen')}}">
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
							Crear mascota
						</button>
					</div>
					{{-- TODO: Cerrar formulario --}}
				</form>
			</div>
		</div>
	</div>
</div>
@endsection