<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FiestasController extends Controller
{
    //
	private $arrayFiestas = array(		
		array(
			'nombre' => 'La Patrona',
			'localizacion' => 'Torrelavega',
			'fechaInicio' => '08-09',
			'fechaFin' => '08-18',
			'imagen' => 'patrona.png',	
			'mapa' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23212.80597811041!2d-4.063760120956224!3d43.34354301450583!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd493e72ed01f43d%3A0xb11dcb6f47adc1a3!2s39300%20Torrelavega%2C%20Cantabria!5e0!3m2!1ses!2ses!4v1580828601205!5m2!1ses!2ses'		
		),
		array(
			'nombre' => 'Carnaval de Cabezon de la sal',
			'localizacion' => 'Cabezon de la sal',
			'fechaInicio' => '08-03',
			'fechaFin' => '10-03',
			'imagen' => 'cabezon.jpg',
			'mapa' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11613.971669553714!2d-4.242476557883372!3d43.3039398661556!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd490e2750df1933%3A0xa845d04dfca82bb1!2sCabez%C3%B3n%20de%20la%20Sal%2C%20Cantabria!5e0!3m2!1ses!2ses!4v1580828763042!5m2!1ses!2ses'
		),
		array(
			'nombre' => 'El cristo',
			'localizacion' => 'Comillas',
			'fechaInicio' => '16-07',
			'fechaFin' => '16-07',
			'imagen' => 'cristo.jpg',
			'mapa' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11598.327924229396!2d-4.301257307845934!3d43.38576386610045!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd490cc070ceb27f%3A0x3dc12b18af6e044d!2sComillas%2C%20Cantabria!5e0!3m2!1ses!2ses!4v1580828802858!5m2!1ses!2ses'
		),		
	);  	

    public function getTodas()
	{
		// $mascotas = Mascota::all();
		return view('fiestas.index', array("fiestas" => $this->arrayFiestas));
	}

	public function getVer($id)
	{    	
		// $mascota = Mascota::findOrFail($id);
		return view('fiestas.mostrar', array('fiestas' => $this->arrayFiestas[$id]));
	}
}
