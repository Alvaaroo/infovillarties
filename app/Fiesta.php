<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fiesta extends Model
{
    //
    //
	protected $table = 'Fiestas';
	protected $primaryKey = 'id';
	public $timestamps = false;

}
